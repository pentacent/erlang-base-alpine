# Erlang Base on Alpine Linux

This is a minimal Alpine image with all dependencies necessary for running
Erlang.

This image is intended to be used as the base for images that contain
Erlang/Elixir applications packaged as Releases. ERTS is **not** included
in this image.

This image is inspired by [bitwalker/alpine-erlang](https://hub.docker.com/r/bitwalker/alpine-erlang/)
which also includes the tools necessary to build Erlang applications.